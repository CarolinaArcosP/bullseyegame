//
//  ViewController.swift
//  BullsEye
//
//  Created by Carolina Arcos on 11/23/17.
//  Copyright © 2017 Condor Labs. All rights reserved.
//

import UIKit
import QuartzCore //Core animation

class ViewController: UIViewController {
    var currentValue: Int = 0 //Instance variable
    var targetValue: Int = 0
    var score: Int = 0
    var round = 0
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var targetValueLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    

    override func viewDidLoad() { //The view controller isn't visible yet
        super.viewDidLoad()
        
        let thumbImageNormal = UIImage(named: "SliderThumb-Normal")!
        slider.setThumbImage(thumbImageNormal, for: .normal)
        
        let thumbImageHighlighted = #imageLiteral(resourceName: "SliderThumb-Highlighted")
        slider.setThumbImage(thumbImageHighlighted, for: .highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        let trackLeftImage = UIImage(named: "SliderTrackLeft")!
        let trackLeftResizable = trackLeftImage.resizableImage(withCapInsets: insets)
        slider.setMinimumTrackImage(trackLeftResizable, for: .normal)
        
        let trackRightImage = #imageLiteral(resourceName: "SliderTrackRight")
        let trackRightResizable = trackRightImage.resizableImage(withCapInsets: insets)
        slider.setMaximumTrackImage(trackRightResizable, for: .normal)
        
        startNewRound()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showAlert() {
        let difference: Int = abs(targetValue - currentValue)
        let points: Int = 100 - difference == 100 ? 200 : 100 - difference
        score += points
        
        var title : String
        switch points {
        case _ where points == 200:
            title = "WOW!"
        case _ where points > 90:
            title = "Almost perfect, but not"
        case _ where points > 50:
            title = "It could be better"
        default:
            title = "It was for the other side"
        }
        
        let message = "[\(currentValue)]\n" +
                        "You scored \(points) points"
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert) //Popup
        let action = UIAlertAction(title: "OK", style: .default,
                                   handler: { action in self.startNewRound() })
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sliderMoved(_ slider: UISlider) {
        currentValue = lround(Double(slider.value))
    }
    
    @IBAction func startOver() {
        score = 0
        round = 0
        startNewRound()
        
        let transition = CATransition()
        transition.type = kCATransitionFade
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        view.layer.add(transition, forKey: nil)
    }
    
    func startNewRound() {
        targetValue = 1 + Int(arc4random_uniform(100))
        currentValue = 50
        slider.value = Float(currentValue)
        round += 1
        
        updateLabels()
    }
    
    
    
    func updateLabels() {
        targetValueLabel.text = String(targetValue)
        scoreLabel.text = String(score)
        roundLabel.text = String(round)
    }
}

